@extends('layouts.app')
@section('title', 'Halaman Food')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/food/create') }}" method="post">
            @csrf
            <div class="mb-3">
                <label>Nama</label>
                <input type="text" class="form-control @if($errors->first('name')) is-invalid @endif" name="name">
                <span class="error invalid-feedback">{{ $errors->first('name') }}></span>
            </div> 
            <div class="mb-3">
                <label>Harga</label>
                <input type="text" class="form-control @if($errors->first('price')) is-invalid @endif" name="price">
                <span class="error invalid-feedback">{{ $errors->first('price') }}></span>
            <div class="mb-3">
                <label>Deskripsi</label>
                <textarea name="description" class="form-control"></textarea>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection