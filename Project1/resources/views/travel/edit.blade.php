@extends('layouts.app')
@section('title', 'Halaman Travel')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/travel/update') }}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $travel->id }}" >
            <div class="mb-3">
                <label>Nama</label>
                <input type="text" class="form-control" name="title" value="{{ $travel->title }}">
            </div> 
            <div class="mb-3">
                <label>Harga</label>
                <input type="text" class="form-control" name="price" value="{{ $travel->price }}">
            <div class="mb-3">
                <label>Deskripsi</label>
                <textarea name="description" class="form-control">{{ $travel->description }}</textarea>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection