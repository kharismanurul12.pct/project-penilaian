<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Food;

class FoodController extends Controller
{
    public function index(){
        $data = Food::orderBy('id','desc')->get();
        return view('food.index', ['data' => $data]);
    }

    public function create(){
        return view('food.create');
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'price' => 'required|numeric'
        ]);

        if($validator->fails()){
            return redirect('/food/add')
            ->withErrors($validator)
            ->withInput();
        }else{
        $food = New Food();
        $food->name = $request->name;
        $food->price = $request->price;
        $food->description = $request->description;
        $food->save(); 
        return redirect('/food');
        }
    }

    public function delete($id){
        $food = Food::where('id',$id)->delete();
        return redirect('/food');
    }

    public function edit($id){
        $food = Food::where('id',$id)->first();
        return view('food.edit',['food' => $food]);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'price' => 'required|numeric'
        ]);

        if($validator->fails()){
            return redirect('/food/edit'.$request->id)
            ->withErrors($validator)
            ->withInput();
        }else{
        $food = Food::where('id',$request->id)->update(
            [
                'name' => $request->name,
                'price' => $request->price,
                'description' => $request->description,
            ]
        
        );
        return redirect('/food');
    }
}
}
