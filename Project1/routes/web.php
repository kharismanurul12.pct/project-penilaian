<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\EditorController;
use App\Http\Controllers\JurnalController;
use App\Http\Controllers\PenilaianController;
use App\Http\Controllers\ReviewerController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/biodata', function () {
    return view('biodata');
});
Route::get('/', [PageController::class, 'home']);
Route::get('/hasilpenilaian', [PageController::class, 'hasilpenilaian']);
Route::get('/informasi', [PageController::class, 'infromasi']);


Route::group(['prefix' => '/news'], function (){
        Route::get('/', [NewsController::class,'index']);
        Route::get('/add', [NewsController::class,'create']);
        Route::post('/create', [NewsController::class,'save']);
        Route::get('/delete/{id}', [NewsController::class,'delete']);
        Route::get('/edit/{id}', [NewsController::class,'edit']);
        Route::post('/update', [NewsController::class,'update']);
        Route::get('/detail/{id}', [NewsController::class,'detail']);
    });

    Route::group(['prefix' => '/food'], function (){
        Route::get('/', [FoodController::class,'index']);
        Route::get('/add', [FoodController::class,'create']);
        Route::post('/create', [FoodController::class,'save']);
        Route::get('/delete/{id}', [FoodController::class,'delete']);
        Route::get('/edit/{id}', [FoodController::class,'edit']);
        Route::post('/update', [FoodController::class,'update']);
    });

    Route::group(['prefix' => '/travel'], function (){
        Route::get('/', [TravelController::class,'index']);
        Route::get('/add', [TravelController::class,'create']);
        Route::post('/create', [TravelController::class,'save']);
        Route::get('/delete/{id}', [TravelController::class,'delete']);
        Route::get('/edit/{id}', [TravelController::class,'edit']);
        Route::post('/update', [TravelController::class,'update']);
    });

    Route::group(['prefix' => '/product'], function (){
        Route::get('/', [ProductController::class,'index']);
        Route::get('/add', [ProductController::class,'create']);
        Route::post('/create', [ProductController::class,'save']);
        Route::get('/delete/{id}', [ProductController::class,'delete']);
        Route::get('/edit/{id}', [ProductController::class,'edit']);
        Route::post('/update', [ProductController::class,'update']);
    });
    

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
