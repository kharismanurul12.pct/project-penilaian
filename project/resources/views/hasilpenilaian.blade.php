@extends('layouts.app')
@section('title', 'Halaman Penilaian')
@section('main')
    @guest
        <div class="container">
            <div class="row mt-3 mb-3">
                @foreach($penilaian as $penilaian)
                <div class="col mb-3">
                    <div class="card">
                        <div class="card-header">
                            <h2>{{ $penilaian->title }}</h2> 
                        </div>
                        <div class="card-body">
                        {{ $penilaian->name }}
                        <hr>
                        {{ $penilaian->pencantuman }}   
                        <hr>         
                        {{ $penilaian->abstrak }}      
                        <hr>      
                        {{ $penilaian->ktkunci }}   
                        <hr>         
                        {{ $penilaian->sistematika }}  
                        <hr>          
                        {{ $penilaian->pemanfaatan }}   
                        <hr>         
                        {{ $penilaian->pengacuan }}   
                        <hr>         
                        {{ $penilaian->dfpustaka }}  
                        <hr>          
                        {{ $penilaian->istilah }}    
                        <hr>        
                        {{ $penilaian->makna }}      
                        <hr>      
                        {{ $penilaian->dampak }}    
                        <hr>        
                        {{ $penilaian->nisbah }}   
                        <hr>         
                        {{ $penilaian->kemutakhiran }}  
                        <hr>          
                        {{ $penilaian->hasil }}    
                        <hr>        
                        {{ $penilaian->penyimpulan }}  
                        <hr>          
                        {{ $penilaian->plagiat }} 
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>

        @else
        <div class="container">
            <div class="row mt-3 mb-3">
                @foreach($data as $penilaian)
                <div class="col-6 mb-3">
                    <div class="card">
                        <div class="card-header">
                        <h3>{{ $penilaian->title }}</h3>
                        </div>
                        <div class="card-body">          
                        {{ $penilaian->pencantuman }}   
                        <hr>         
                        {{ $penilaian->abstrak }}      
                        <hr>      
                        {{ $penilaian->ktkunci }}   
                        <hr>         
                        {{ $penilaian->sistematika }}  
                        <hr>          
                        {{ $penilaian->pemanfaatan }}   
                        <hr>         
                        {{ $penilaian->pengacuan }}   
                        <hr>         
                        {{ $penilaian->dfpustaka }}  
                        <hr>          
                        {{ $penilaian->istilah }}    
                        <hr>        
                        {{ $penilaian->makna }}      
                        <hr>      
                        {{ $penilaian->dampak }}    
                        <hr>        
                        {{ $penilaian->nisbah }}   
                        <hr>         
                        {{ $penilaian->kemutakhiran }}  
                        <hr>          
                        {{ $penilaian->hasil }}    
                        <hr>        
                        {{ $penilaian->penyimpulan }}  
                        <hr>          
                        {{ $penilaian->plagiat }} 
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    @endguest
@endsection