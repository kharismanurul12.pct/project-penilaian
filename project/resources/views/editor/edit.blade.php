@extends('layouts.app')
@section('title', 'Halaman Editor')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/editor/update') }}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $editor->id }}" >
            <div class="mb-3">
                <label>Nama Editor</label>
                <input type="text" class="form-control  @if($errors->first('name')) is-invalid @endif" name="name" value="{{ $editor->name }}">
                <span class="error invalid-feedback">{{ $errors->first('name') }}></span>
            </div> 
            <div class="mb-3">
                <label>NIDN</label>
                <input type="text" class="form-control  @if($errors->first('nidn')) is-invalid @endif" name="nidn" value="{{ $editor->nidn }}">
                <span class="error invalid-feedback">{{ $errors->first('nidn') }}></span>
            <div class="mb-3">
                <label>Bidang Keilmuan</label>
                <input type="text" class="form-control  @if($errors->first('keilmuan')) is-invalid @endif" name="keilmuan" value="{{ $editor->keilmuan }}">
                <span class="error invalid-feedback">{{ $errors->first('keilmuan') }}></span>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection