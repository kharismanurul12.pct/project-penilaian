@extends('layouts.app')
@section('title', 'Halaman Editor')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/editor/create') }}" method="post">
            @csrf
            <div class="mb-3">
                <label>Nama Editor</label>
                <input type="text" class="form-control @if($errors->first('name')) is-invalid @endif" name="name">
                <span class="error invalid-feedback">{{ $errors->first('name') }}></span>
            </div> 
            <div class="mb-3">
                <label>NIDN</label>
                <input type="text" class="form-control @if($errors->first('judul')) is-invalid @endif" name="nidn">
                <span class="error invalid-feedback">{{ $errors->first('judul') }}></span>
            <div class="mb-3">
                <label>Bidang Keilmuan</label>
                <input type="text" class="form-control @if($errors->first('keilmuan')) is-invalid @endif" name="keilmuan">
                <span class="error invalid-feedback">{{ $errors->first('keilmuan') }}></span>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection