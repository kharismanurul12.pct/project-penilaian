@extends('layouts.app')
@section('title', 'Halaman Editor')
@section('main')
    @guest
        <div class="container">
            <div class="row mt-3 mb-3">
                @foreach($data as $editor)
                <div class="col-3 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <b>{{ $editor->name }}</b> 
                        </div>
                        <div class="card-body">
                            {{ $editor->nidn }}
                            {{ $editor->keilmuan }}
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>

        @else
        <div class="container">
            <div class="row mt-3 mb-3">
                <a class="btn btn-primary mb-3" href="{{ url('/editor/add') }}">Tambah Data</a>
                @foreach($data as $editor)
                <div class="col-3 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <b>{{ $editor->name }}</b> 
                        </div>
                        <div class="card-body">
                            {{ $editor->nidn }}
                            <hr>
                            {{ $editor->keilmuan }}
                        </div>
                        <div class="card-footer">
                        <a href="{{ url('/editor/edit/'.$editor->id) }}" class="btn btn-warning btn-sm">Edit</a>
                        <a href="{{ url('/editor/delete/'.$editor->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    @endguest
@endsection