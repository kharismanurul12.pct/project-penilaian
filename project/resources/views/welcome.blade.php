@extends('layouts.app')

@section('title', 'Penilaian Jurnal')
@section('main')

<div class="card text-center">
  <div class="card-header mt-4">
    <h1>Institut Teknologi dan Bisnis PalComTech</h1>
  </div>
  <div class="card-body">
    <img src="/img/Logo.jpg" alt="" width=250 style="display:block; margin:auto;" class="mt-3">
  </div>
  <div class="card-footer text-muted">
    <h2>Lembaga Penelitian dan Pengabdian Masyarakat</h2>
  </div>
</div>

@endsection