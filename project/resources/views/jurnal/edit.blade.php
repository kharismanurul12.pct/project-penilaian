@extends('layouts.app')
@section('title', 'Halaman Jurnal')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/jurnal/update') }}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $jurnal->id }}" >
            <div class="mb-3">
                <label>Nama Jurnal</label>
                <input type="text" class="form-control  @if($errors->first('name')) is-invalid @endif" name="name" value="{{ $jurnal->name }}">
                <span class="error invalid-feedback">{{ $errors->first('name') }}></span>
            </div> 
            <div class="mb-3">
                <label>Judul Jurnal</label>
                <input type="text" class="form-control  @if($errors->first('title')) is-invalid @endif" name="title" value="{{ $jurnal->title }}">
                <span class="error invalid-feedback">{{ $errors->first('title') }}></span>
            <div class="mb-3">
                <label>File Jurnal</label>
                <textarea name="description" class="form-control">{{ $jurnal->description }}</textarea>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection