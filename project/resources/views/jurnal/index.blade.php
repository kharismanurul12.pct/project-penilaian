@extends('layouts.app')
@section('title', 'Halaman Jurnal')
@section('main')
    @guest
        <div class="container">
            <div class="row mt-3 mb-3">
                @foreach($data as $jurnal)
                <div class="col-3 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <b>{{ $jurnal->title }}</b> 
                        </div>
                        <div class="card-body">
                            {{ $jurnal->name }}
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>

        @else
        <div class="container">
            <div class="row mt-3 mb-3">
                <a class="btn btn-primary mb-3" href="{{ url('/jurnal/add') }}">Tambah Data</a>
                @foreach($data as $jurnal)
                <div class="col-3 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <b>{{ $jurnal->title }}</b> 
                        </div>
                        <div class="card-body">
                            {{ $jurnal->name }}
                            {{ $jurnal->file }}
                        </div>
                        <div class="card-footer">
                        <a href="{{ url('/jurnal/edit/'.$jurnal->id) }}" class="btn btn-warning btn-sm">Edit</a>
                        <a href="{{ url('/jurnal/delete/'.$jurnal->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    @endguest
@endsection