@extends('layouts.app')
@section('title', 'Halaman Penilaian')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/penilaian/create') }}" method="post">
            @csrf
            <div class="card text-center">
                <div class="card-body">
                    Form Keterangan Kelengkapan Paper Teknomatika
                </div>
            </div>
            <div class="mb-3 mt-3">
                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Unsur Editing</th>
                        <th>Keterangan</th>
                        <th>Komentar</th>
                    </tr>
                    <tr>
                        <td>1.</td>
                        <td>Judul Artikel</td>
                        <td>Maksimal 12 (dua belas) kata dalam Judul Bahasa Indonesia dan Bahasa Inggris</td>
                        <td><input type="text" class="form-control" name="title"></td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Pencantuman Nama Penulis dan Lembaga Penulis</td>
                        <td>Urutan pencantuman:
                            <br>Baris 1: Penulis
                            <br>Baris 2: Lembaga
                            <br>Baris 3: Jurusan
                            <br>Baris 4: Email
                        </td>
                        <td><textarea name="pencantuman" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>Abstrak</td>
                        <td>Dalam Bahasa Indonesia dan Bahasa Inggris yang baik, jumlah 150-200
                            kata. Isi terdiri dari latar belakang, tujuan metode, hasil, dan kesimpulan.
                            isi tertuang dengan kalimat yang jelas.
                        </td>
                        <td><textarea name="abstrak" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Kata Kunci</td>
                        <td>Maksimal 5 kata kunci terpenting dalam paper</td>
                        <td><textarea name="ktkunci" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td>Sistematika Penulisan</td>
                        <td>Terdiri dari pendahuluan (berisi latar belakang, tujuan manfaat, dan penelitian 
                            terdahulu), metode penelitian, hasil dan pembahasan, kesimpulan, 
                            ucapan terima kasih (jika ada) dan daftar pustaka</td>
                        <td><textarea name="sistematika" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>Pemanfaatan Instrumen Pendukung</td>
                        <td>Pemanfaatan Instrumen Pendukung seperti gambar dan tabel</td>
                        <td><textarea name="pemanfaatan" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td>Cara Pengacuan dan Pengutipan</td>
                        <td>Pengutipan menggunakan format IEEE dan Maksimal 10 Tahun Terahir</td>
                        <td><textarea name="pengacuan" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>8.</td>
                        <td>Penyusunan Daftar Pustaka</td>
                        <td>Penyusunan daftar pustaka menggunakan format IEEE</td>
                        <td><textarea name="dfpustaka" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td>Peristilahan dan Kebahasaan</td>
                        <td>Jurnal harus menggunakan bahasa baku sesuai EYD</td>
                        <td><textarea name="istilah" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td>Makna Sumbangan bagi Kemajuan</td>
                        <td></td>
                        <td><textarea name="makna" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>11.</td>
                        <td>Dampak Ilmiah</td>
                        <td></td>
                        <td><textarea name="dampak" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>12.</td>
                        <td>Nisbah Sumber Acuan Primer berbanding Sumber lainnya</td>
                        <td>Sumber acuan minimal 7 jurnal terdahulu setiap paper</td>
                        <td><textarea name="nisbah" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>13.</td>
                        <td>Derajat Kemutakhiran Pustaka Acuan</td>
                        <td>Derajat Kemutakhiran Pustaka Acuan</td>
                        <td><textarea name="kemutakhiran" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>14.</td>
                        <td>Hasil dan Pembahasan</td>
                        <td>Kesesuaian isi hasil pembahasan dengan judul paper</td>
                        <td><textarea name="hasil" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>15.</td>
                        <td>Penyimpulan</td>
                        <td>Sangat jelas relevasinya dengan latar belakang dan pembahasan, 
                            dirumuskan dengan singkat dan menjawan tujuan penelitian</td>
                        <td><textarea name="penyimpulan" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                    <tr>
                        <td>16.</td>
                        <td>Unsul Plagiat</td>
                        <td>Maksimal Plagiasi yang diterima 25%</td>
                        <td><textarea name="plagiat" class="form-control" cols="300" rows="5"></textarea></td>
                    </tr>
                </table>
            </div>
            
            </div>
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>

@endsection