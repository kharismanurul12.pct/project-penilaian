@extends('layouts.app')
@section('title', 'Halaman Reviewer')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/reviewer/create') }}" method="post">
            @csrf
            <div class="mb-3">
                <label>Nama Reviewer</label>
                <input type="text" class="form-control @if($errors->first('name')) is-invalid @endif" name="name">
                <span class="error invalid-feedback">{{ $errors->first('name') }}></span>
            </div> 
            <div class="mb-3">
                <label>Bidang Keilmuan</label>
                <input type="text" class="form-control @if($errors->first('keilmuan')) is-invalid @endif" name="keilmuan">
                <span class="error invalid-feedback">{{ $errors->first('keilmuan') }}></span>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection