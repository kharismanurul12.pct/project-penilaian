@extends('layouts.app')
@section('title', 'Halaman Reviewer')
@section('main')
    @guest
        <div class="container">
            <div class="row mt-3 mb-3">
                @foreach($data as $reviewer)
                <div class="col-3 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <b>{{ $reviewer->name }}</b> 
                        </div>
                        <div class="card-body">
                            {{ $reviewer->keilmuan }}
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>

        @else
        <div class="container">
            <div class="row mt-3 mb-3">
                <a class="btn btn-primary mb-3" href="{{ url('/reviewer/add') }}">Tambah Data</a>
                @foreach($data as $reviewer)
                <div class="col-3 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <b>{{ $reviewer->name }}</b> 
                        </div>
                        <div class="card-body">
                            {{ $reviewer->keilmuan }}
                        </div>
                        <div class="card-footer">
                        <a href="{{ url('/reviewer/edit/'.$reviewer->id) }}" class="btn btn-warning btn-sm">Edit</a>
                        <a href="{{ url('/reviewer/delete/'.$reviewer->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    @endguest
@endsection