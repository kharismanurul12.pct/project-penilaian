<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{   
    function home(){
        return view('welcome');
    }
    function infromasi(){
        return view('informasi');
    }
    function hasilpenilaian(){
        return view('hasilpenilaian');
    }
    function editor(){
        return view('editor');
    }
    function jurnal(){
        return view('jurnal');
    }
    function reviewer(){
        return view('reviewer');
    }
    function penilaian(){
        return view('penilaian');
    }
}
