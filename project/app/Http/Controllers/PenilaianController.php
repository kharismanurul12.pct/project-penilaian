<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Penilaian;

class PenilaianController extends Controller
{
    public function index(){
        $data = Penilaian::get();
        return view('penilaian.index', ['data' => $data]);
    }

    public function create(){
        return view('penilaian.create');
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(),[
            'title' => 'required|string|max:255',
            'pencantuman' => 'required|string',
            'abstrak' => 'required|string',
            'ktkunci' => 'required|string',
            'sistematika' => 'required|string',
            'pemanfaatan' => 'required|string',
            'pengacuan' => 'required|string',
            'dfpustaka' => 'required|string',
            'istilah' => 'required|string',
            'makna' => 'required|string',
            'dampak' => 'required|string',
            'nisbah' => 'required|string',
            'kemutakhiran' => 'required|string',
            'hasil' => 'required|string',
            'penyimpulan' => 'required|string',
            'plagiat' => 'required|string',
        ]);

        if($validator->fails()){
            return redirect('/penilaian/add')
            ->withErrors($validator)
            ->withInput();
        }else{
        $penilaian = New Penilaian();
        $penilaian->title = $request->title;
        $penilaian->pencantuman = $request->pencantuman;
        $penilaian->abstrak = $request->abstrak;
        $penilaian->ktkunci = $request->ktkunci;
        $penilaian->sistematika = $request->sistematika;
        $penilaian->pemanfaatan = $request->pemanfaatan;
        $penilaian->pengacuan = $request->pengacuan;
        $penilaian->dfpustaka = $request->dfpustaka;
        $penilaian->istilah = $request->istilah;
        $penilaian->makna = $request->makna;
        $penilaian->dampak = $request->dampak;
        $penilaian->nisbah = $request->nisbah;
        $penilaian->kemutakhiran = $request->kemutakhiran;
        $penilaian->hasil = $request->hasil;
        $penilaian->penyimpulan = $request->penyimpulan;
        $penilaian->plagiat = $request->plagiat;
        $penilaian->save(); 
        return redirect('/penilaian');
        }
    }

    public function delete($id){
        $penilaian = Penilaian::where('id',$id)->delete();
        return redirect('/penilaian');
    }

    public function edit($id){
        $penilaian = Penilaian::where('id',$id)->first();
        return view('penilaian.edit',['penilaian' => $penilaian]);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'pencantuman' => 'required|string',
            'abstrak' => 'required|string',
            'ktkunci' => 'required|string',
            'sistematika' => 'required|string',
            'pemanfaatan' => 'required|string',
            'pengacuan' => 'required|string',
            'dfpustaka' => 'required|string',
            'istilah' => 'required|string',
            'makna' => 'required|string',
            'dampak' => 'required|string',
            'nisbah' => 'required|string',
            'kemutakhiran' => 'required|string',
            'hasil' => 'required|string',
            'penyimpulan' => 'required|string',
            'plagiat' => 'required|string',
        ]);

        if($validator->fails()){
            return redirect('/penilaian/edit'.$request->id)
            ->withErrors($validator)
            ->withInput();
        }else{
        $penilaian = Penilaian::where('id',$request->id)->update(
            [
                'title' => $request->title,
                'pencantuman' => $request->pencantuman,
                'abstrak' => $request->abstrak,
                'ktkunci' => $request->ktkunci,
                'sistematika' => $request->sistematika,
                'pemanfaatan' => $request->pemanfaatan,
                'pengacuan' => $request->pengacuan,
                'dfpustaka' => $request->dfpustaka,
                'istilah' => $request->istilah,
                'makna' => $request->makna,
                'dampak' => $request->dampak,
                'nisbah' => $request->nisbah,
                'kemutakhiran' => $request->kemutakhiran,
                'hasil' => $request->hasil,
                'penyimpulan' => $request->penyimpulan,
                'plagiat' => $request->plagiat,
            ]
        
        );
        return redirect('/penilaian');
        }
    }

}
