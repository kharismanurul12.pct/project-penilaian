<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Editor;

class EditorController extends Controller
{
    public function index(){
        $data = Editor::get();
        return view('editor.index', ['data' => $data]);
    }

    public function create(){
        return view('editor.create');
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'nidn' => 'required|string',
            'keilmuan' => 'required|string'
        ]);

        if($validator->fails()){
            return redirect('/editor/add')
            ->withErrors($validator)
            ->withInput();
        }else{
        $editor = New Editor();
        $editor->name = $request->name;
        $editor->nidn = $request->nidn;
        $editor->keilmuan = $request->keilmuan;
        $editor->save(); 
        return redirect('/editor');
        }
    }

    public function delete($id){
        $editor = Editor::where('id',$id)->delete();
        return redirect('/editor');
    }

    public function edit($id){
        $editor = Editor::where('id',$id)->first();
        return view('editor.edit',['editor' => $editor]);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'nidn' => 'required|string',
            'keilmuan' => 'required|string'
        ]);

        if($validator->fails()){
            return redirect('/editor/edit'.$request->id)
            ->withErrors($validator)
            ->withInput();
        }else{
        $editor = Editor::where('id',$request->id)->update(
            [
                'name' => $request->name,
                'nidn' => $request->nidn,
                'keilmuan' => $request->keilmuan,
            ]
        
        );
        return redirect('/editor');
    }
}
}