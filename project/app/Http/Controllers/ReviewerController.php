<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Reviewer;

class ReviewerController extends Controller
{
    public function index(){
        $data = Reviewer::get();
        return view('reviewer.index', ['data' => $data]);
    }

    public function create(){
        return view('reviewer.create');
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'keilmuan' => 'required|string'
        ]);

        if($validator->fails()){
            return redirect('/reviewer/add')
            ->withErrors($validator)
            ->withInput();
        }else{
        $reviewer = New Reviewer();
        $reviewer->name = $request->name;
        $reviewer->keilmuan = $request->keilmuan;
        $reviewer->save(); 
        return redirect('/reviewer');
        }
    }

    public function delete($id){
        $reviewer = Reviewer::where('id',$id)->delete();
        return redirect('/reviewer');
    }

    public function edit($id){
        $reviewer = Reviewer::where('id',$id)->first();
        return view('reviewer.edit',['reviewer' => $reviewer]);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'keilmuan' => 'required|string'
        ]);

        if($validator->fails()){
            return redirect('/reviewer/edit'.$request->id)
            ->withErrors($validator)
            ->withInput();
        }else{
        $reviewer = Reviewer::where('id',$request->id)->update(
            [
                'name' => $request->name,
                'keilmuan' => $request->keilmuan,
            ]
        
        );
        return redirect('/reviewer');
    }
}
}