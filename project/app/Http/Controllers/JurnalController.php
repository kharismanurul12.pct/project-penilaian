<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Jurnal;

class JurnalController extends Controller
{
    public function index(){
        $data = Jurnal::get();
        return view('jurnal.index', ['data' => $data]);
    }

    public function create(){
        return view('jurnal.create');
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'title' => 'required|string'
        ]);

        if($validator->fails()){
            return redirect('/jurnal/add')
            ->withErrors($validator)
            ->withInput();
        }else{
        $jurnal = New Jurnal();
        $jurnal->name = $request->name;
        $jurnal->title = $request->title;
        $jurnal->file = $request->file;
        $jurnal->save(); 
        return redirect('/jurnal');
        }
    }

    public function delete($id){
        $jurnal = Jurnal::where('id',$id)->delete();
        return redirect('/jurnal');
    }

    public function edit($id){
        $jurnal = Jurnal::where('id',$id)->first();
        return view('jurnal.edit',['jurnal' => $jurnal]);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'title' => 'required|string'
        ]);

        if($validator->fails()){
            return redirect('/jurnal/edit'.$request->id)
            ->withErrors($validator)
            ->withInput();
        }else{
        $jurnal = Jurnal::where('id',$request->id)->update(
            [
                'name' => $request->name,
                'title' => $request->title,
                'file' => $request->file,
            ]
        
        );
        return redirect('/jurnal');
    }
}
}