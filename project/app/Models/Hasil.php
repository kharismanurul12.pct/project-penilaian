<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasil extends Model
{
    use HasFactory;

    protected $table = hasils;
    protected $fillable = ['name','title','pencantuman','abstrak','ktkunci','sistematika','pemanfaatan',
    'pengacuan','dfpustaka','istilah','makna','dampak','nisbah','kemutakhiran','hasil','penyimpulan','plagiat',];
}
