<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\JurnalController;
use App\Http\Controllers\EditorController;
use App\Http\Controllers\ReviewerController;
use App\Http\Controllers\PenilaianController;
use App\Http\Controllers\HasilController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PageController::class, 'home']);
Route::get('/informasi', [PageController::class, 'informasi']);
Route::get('/hasilpenilaian', [PageController::class, 'hasilpenilaian']);
Route::get('/editor', [PageController::class, 'editor']);
Route::get('/jurnal', [PageController::class, 'jurnal']);
Route::get('/reviewer', [PageController::class, 'reviewer']);
Route::get('/penilaian', [PageController::class, 'penilaian']);


Route::group(['prefix' => '/jurnal'], function (){
    Route::get('/', [JurnalController::class,'index']);
    Route::get('/add', [JurnalController::class,'create']);
    Route::post('/create', [JurnalController::class,'save']);
    Route::get('/delete/{id}', [JurnalController::class,'delete']);
    Route::get('/edit/{id}', [JurnalController::class,'edit']);
    Route::post('/update', [JurnalController::class,'update']);
});

Route::group(['prefix' => '/editor'], function (){
    Route::get('/', [EditorController::class,'index']);
    Route::get('/add', [EditorController::class,'create']);
    Route::post('/create', [EditorController::class,'save']);
    Route::get('/delete/{id}', [EditorController::class,'delete']);
    Route::get('/edit/{id}', [EditorController::class,'edit']);
    Route::post('/update', [EditorController::class,'update']);
});

Route::group(['prefix' => '/reviewer'], function (){
    Route::get('/', [ReviewerController::class,'index']);
    Route::get('/add', [ReviewerController::class,'create']);
    Route::post('/create', [ReviewerController::class,'save']);
    Route::get('/delete/{id}', [ReviewerController::class,'delete']);
    Route::get('/edit/{id}', [ReviewerController::class,'edit']);
    Route::post('/update', [ReviewerController::class,'update']);
});

Route::group(['prefix' => '/penilaian'], function (){
    Route::get('/', [PenilaianController::class,'index']);
    Route::get('/add', [PenilaianController::class,'create']);
    Route::post('/create', [PenilaianController::class,'save']);
    Route::get('/delete/{id}', [PenilaianController::class,'delete']);
    Route::get('/edit/{id}', [PenilaianController::class,'edit']);
    Route::post('/update', [PenilaianController::class,'update']);
    Route::get('/detail', [NewsController::class,'detail']);
    
});

Route::get('/hasilpenilaian', [HasilController::class, 'hasilpenilaian']);
        
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

